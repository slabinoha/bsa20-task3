import { createElement } from '../helpers/domHelper';
import { showModal } from './modal';

export  function showWinnerModal(fighter) {
  const title = 'And the winner is...';
  const bodyElement = createFighterSummary(fighter);
  showModal({ title, bodyElement });
}

function createFighterSummary(fighter) {
  const { name, attack, defense, health, source } = fighter;

  const fighterSummary = createElement({ tagName: 'div', className: 'modal-body' });
  const nameElement = createElement({ tagName: 'p', className: 'fighter-name' });
  const imageElement = createElement({ tagName: 'img', className: 'fighter-image'});

  nameElement.innerText = name;
  imageElement.src = source;
  fighterSummary.append(nameElement);
  fighterSummary.append(imageElement);

  return fighterSummary;
}
