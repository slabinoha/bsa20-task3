export function fight(firstFighter, secondFighter) {
  while (true){
    secondFighter.health-=getDamage(firstFighter,secondFighter);
    firstFighter.health-=getDamage(secondFighter,firstFighter);
    if(secondFighter.health<=0 || firstFighter.health<=0){
      break;
    }
  }
  if(secondFighter.health<=0){
    return firstFighter;
  } else{
    return secondFighter;
  }
}

export function getDamage(attacker, enemy) {
  return getHitPower(attacker)-getBlockPower(enemy);
}

export function getHitPower(fighter) {
  let criticalHitChance=Math.random()+1;
  return fighter.attack*criticalHitChance;
}

export function getBlockPower(fighter) {
  let dodgeChance=Math.random()+1;
  return fighter.defense*dodgeChance;
}
